import megamu.mesh.*;
import controlP5.*;
import processing.video.*;
Movie myMovie;

ControlP5 cp5;
controlP5.Toggle c,d,e,f;
PImage[] images;
PImage image;
int seed,x,y,p;
int particles = 10000;
float power,mvalue,ratio,posx,posy,deltaX,deltaY,tStep;
float startX,startY,endX,endY;
float[][] points;
Delaunay myDelaunay;
float[][] myEdges;
color c1,c2;
boolean re, colorMode,drawDelaunay,lightMode,showGui,play,overlay = true;
int source = 0;
float playback_speed;
float intensity = 0.85;
float bg_x,bg_y;

PImage img;




void setup(){
  //size(1280,720,P2D);
  fullScreen(P3D, 1);
  //pixelDensity(2);
  
  
  img = loadImage("Background.jpg"); //<>//
  //float ratio_screen = width/height;
  //float ratio_image = img.width/img.height;
  img.loadPixels();
  float bg_ratio = max(float(width)/(float)img.width,float(height)/ (float)img.height);
  img.resize(int(img.width*bg_ratio), int(img.height*bg_ratio));
  bg_x = -(img.width - width) / 2.0;
  bg_y = -(img.height - height) / 2.0;
  
  myMovie = new Movie(this, "Source.mp4"); //<>//
  myMovie.loop();
  myMovie.play();
  playback_speed = 1.0;
  myMovie.speed(1.0);
  
  //noLoop();
  power = 2;
  particles = 500;
  
  images = new PImage[8];
  for (int i=0; i< images.length-1;i++){
     images[i] = loadImage("source"+i+".jpg");
     images[i].loadPixels();  
  }
  images[7]=images[0];
  image = images[0];
  
  cp5 = new ControlP5(this);
  cp5.addSlider("source")
     .setPosition(50,20)
     .setSize(50,10)
     .setRange(0,7)
     .setNumberOfTickMarks(7)
     ;
  cp5.addSlider("particles").setPosition(50,50).setRange(0,10000);
  cp5.addSlider("power").setPosition(50,80).setRange(0.5,8.0)
     .setValue(6.0)
  ;
   c= cp5.addToggle("toggle")
     .setPosition(50,110)
     .setSize(50,10)
     .setValue(true)
     .setMode(ControlP5.SWITCH);
  
  d= cp5.addToggle("toggle2")
     .setPosition(50,140)
     .setSize(50,10)
     .setValue(true)
     .setMode(ControlP5.SWITCH);
  
  e= cp5.addToggle("toggle3")
     .setPosition(50,170)
     .setSize(50,10)
     .setValue(true)
     .setMode(ControlP5.SWITCH);
     
  f= cp5.addToggle("toggle4")
     .setPosition(50,200)
     .setSize(50,10)
     .setValue(false)
     .setMode(ControlP5.SWITCH);
     
  //g= cp5.addToggle("toggle5")
  //   .setPosition(50,230)
  //   .setSize(50,10)
  //   .setValue(false)
  //   .setMode(ControlP5.SWITCH);
     
  cp5.addSlider("playback").setPosition(50,260)
      .setRange(-1.0,1.0)
      .setNumberOfTickMarks(17)
      ;
  cp5.addSlider("intensity").setPosition(50,320)
      .setRange(0.0,1.0)
      .setValue(0.85);
      ;
  
  c.setLabel("Color");
  d.setLabel("Draw Delaunay");
  e.setLabel("Light or Dark");
  //f.setLabel("Play");
  f.setLabel("Overlay");
  
}


void draw()
{
// println(re);
   
  if (re == true)
  {
    //if (play)
    //{
      //image = images[7];
      //images[7] = myMovie;
      //myMovie.loadPixels();
    //}
    image = images[source];
    re = false;   
    println(colorMode);
    particles = (int)cp5.getController("particles").getValue();
    power = cp5.getController("power").getValue();
    ratio = min(float(width-50)/(float)image.width,float(height-50)/ (float)image.height);
    posx = (width-image.width*ratio)/2;
    posy = (height-image.height*ratio)/2;
    PImage scaled_image = image;
    scaled_image.resize(int(scaled_image.width*ratio), int(scaled_image.height*ratio));
    
    
    //background(img);
    background(0);
    noFill();
    noStroke();
    //if(colorMode){
      //background(image.get(0,0));
    //}
  
    // Draw Background
    if (overlay)
    {
      image(img, bg_x, bg_y);
    }
    else
    {
      image(scaled_image, posx, posy);
    }
    
    points = new float[particles][2];
    println(image.width,image.height);
    while (particles > 0)
    {
      x = (int)random(image.width);
      y = (int)random(image.height);
      p = x+y*image.width;
      mvalue = map(brightness(image.pixels[p]),0,255,1,0);
      // println(mvalue,value);
     
     
      if(!drawDelaunay)
      {
        fill(255);
      }
      boolean condition;
      if (lightMode)
      {
        condition = random(1.0) < pow(mvalue,power);
      }
      else
      {
        condition = random(1.0) < pow(1 - mvalue,power);
      }
   
  
      if(condition)
      {
        particles--;
        points[particles][0] = x;
        points[particles][1] = y;
      
        if(!drawDelaunay)
        {
            if (colorMode)
            {
              fill(image.get(x,y));
            }
            ellipse(posx+x,posy+y,2,2); 
        }
      }
    }
    
    if (drawDelaunay)
    {
      myDelaunay = new Delaunay( points );
      myEdges = myDelaunay.getEdges();
      //stroke(0);
      beginShape(LINES);
      strokeWeight(1.5);
      for(int i=0; i<myEdges.length; i++)
      {
        
        startX = posx + myEdges[i][0];//* ratio;
        startY = posy + myEdges[i][1];//*ratio;
        endX = posx + myEdges[i][2];//* ratio;
        endY = posy + myEdges[i][3];//* ratio;
        //colorMode(HSB, 255);
        if (colorMode)
        {
          c1 = image.get((int)myEdges[i][0],(int)myEdges[i][1]);
          c2 = image.get((int)myEdges[i][2],(int)myEdges[i][3]);
          //c1 = color(hue(c1),saturation(c1),brightness(c1)*intensity);
          //c2 = color(hue(c1),saturation(c1),brightness(c1)*intensity);
          c1 = color(min(255, red(c1)*intensity), min(255, green(c1)*intensity), min(255, blue(c1)*intensity));
          c2 = color(min(255, red(c2)*intensity), min(255, green(c2)*intensity), min(255, blue(c2)*intensity));
          //float c1_val = (255-brightness(c1))/255.0;
          //float c2_val = (255-brightness(c2))/255.0;
          //c1 = int(c1_val * c1);
          //c1 = int(c2_val * c2);
          
          //gradientLine( startX, startY, endX, endY,c1,c2 );
          stroke(c1);
          vertex(startX, startY);
          stroke(c2);
          vertex(endX, endY);
        }
        else
        {
          stroke(250,200);
          vertex(startX, startY);
          vertex(endX, endY);
          //line(startX, startY, endX, endY);
        }
        //colorMode(RGB, 255);
      }
      endShape();
    }
    
      // fill(255);
      // text(seed, 10, height-10);
  }
}


void gradientLine(float x1, float y1, float x2, float y2, color a, color b)
{
  deltaX = x2-x1;
  deltaY = y2-y1;
  tStep = 1.0/dist(x1, y1, x2, y2);
  for (float t = 0.0; t < 1.0; t += tStep) 
  {
    fill(lerpColor(a, b, t));
    ellipse(x1+t*deltaX,  y1+t*deltaY, 1, 1);
  }
}

void toggle(boolean theFlag) {
  
  if(theFlag==true) {
    colorMode = true;
  } else {
    colorMode = false;
  }
  println("a toggle event.",theFlag);
}

void toggle2(boolean theFlag) {
  
  if(theFlag==true) {
    drawDelaunay = true;
  } else {
    drawDelaunay = false;
  }
  println("a toggle event.",theFlag);
}


void toggle3(boolean theFlag) {
  
  if(theFlag==true) {
    lightMode = true;
  } else {
    lightMode = false;
  }
  println("a toggle event.",theFlag);
}

/*
void toggle5(boolean theFlag) {
  
  if(theFlag==true) {
    play = true;
  } else {
    play = false;
  }
  println("a toggle event.",theFlag);
}
*/
void toggle4(boolean theFlag) {
  
  if(theFlag==true) {
    overlay = true;
  } else {
    overlay = false;
  }
  println("a toggle event.",theFlag);
}


void source(int i) {
  //image = images[i];
  source = i;
  println("a toggle event.",i);
}

void playback(float f) {
  //image = images[i];
  if (f >= -0.01 && f <= 0.01)
  {
    play = false;
    myMovie.pause();
  }
  else
  {
    play = true;
    myMovie.play();
  }
  playback_speed = f;
  myMovie.speed(f);
  println("a toggle event.",f);
}

void intensity(float f) {
  intensity = f;
  println("a toggle event.",f);
}


void mousePressed(){
  println("ciao");
   seed = (int)random(10000);
   randomSeed(seed);
   re = true;
}

void keyPressed(){
  if (key == 'h'){
    showGui = !showGui;
     cp5.setVisible(showGui);  
  }
  if (key == 's'){
    saveFrame();
  }
}

// Called every time a new frame is available to read
void movieEvent(Movie m) {
  if (play)
  {
    m.read();
    images[7] = myMovie.get();
    //images[7].loadPixels();
    //image = images[7];
    re = true;
  }
}

@Override void exit() {
  myMovie.stop();
  super.exit();
}
